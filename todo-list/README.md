# Todo-list

Little pet-project with authentication and persistent storage.

## Technologies used
+ React
+ Webpack
+ PHP (Phalcon framework)
+ PostgreSQL
+ Nginx
+ Docker

## Running locally 
Start the project with command: `./run.sh`

Head to the [localhost:8080](localhost:8080)

NOTE: you have to have docker & docker-compose set up on your machine

## :tada: Screenshots:

![image](/uploads/a1fb3f47286a3e940911e555da1f61c9/image.png)


![image](/uploads/3286beaac3fe79a54ac25f6bf9182282/image.png)